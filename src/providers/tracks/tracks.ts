import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

/*
  Generated class for the TracksProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class TracksProvider {
  url = 'assets/wikiloops-data/';

  constructor(public http: Http) {
    console.log('Hello TracksProvider Provider');
  }

  //get to retrieve JSON data from URL as observable
  //map converts result into JSON decoded version of result
  getTracks() { return this.http.get(this.url + 'wikiloops_last_50.json').map(res => res.json()); }

  getAlbumOfTheDay() { return this.http.get(this.url + 'wikiloops_album_of_the_day.json').map(res => res.json()); }

}
