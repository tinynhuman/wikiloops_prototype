import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TracksProvider } from '../../providers/tracks/tracks';
import { TrackListPage } from '../track-list/track-list';
import { TrackDetailPage } from '../track-detail/track-detail';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  albumOfTheDay: any;
  latestTracks: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams, 
    private tracksProvider: TracksProvider
  ) {

  }
  ionViewWillEnter() {
    this.tracksProvider.getAlbumOfTheDay().subscribe(album => { this.albumOfTheDay = album; });
    this.tracksProvider.getTracks().subscribe(tracks => { this.latestTracks = tracks.tracks.slice(0,5); });
  }

  openPlaylist(){
    //this.navCtrl.push(PlaylistPage);
  }
  openTracklist(){
    this.navCtrl.push(TrackListPage);
  }
  openAlbum(url:string){
    var win = window.open(url, '_blank');
    win.focus();
  }
  openProfile(url:string){
    var win = window.open(url, '_blank');
    win.focus();
  }
  openPost(url:string){
    var win = window.open(url, '_blank');
    win.focus();
  }
  trackTapped(event, track) { this.navCtrl.push(TrackDetailPage, { track: track }); }


}
