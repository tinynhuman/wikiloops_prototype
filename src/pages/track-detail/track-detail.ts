import { Component, ChangeDetectorRef } from '@angular/core';
import { Events } from 'ionic-angular';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
//import { ITrackConstraint } from 'ionic-audio';
//import { Storage } from '@ionic/storage';

/**
 * Generated class for the TrackDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-track-detail',
  templateUrl: 'track-detail.html',
})
export class TrackDetailPage {
  selectedTrack: any;
  //myTracks: ITrackConstraint[];
  //playlist: ITrackConstraint[] = [];
  checkTrack: number = -1;

  currentIndex: number = -1;
  //currentTrack: ITrackConstraint;

  constructor(
    private _cdRef: ChangeDetectorRef,
    public events: Events,
    public navCtrl: NavController,
    public navParams: NavParams,
   // private storage: Storage
  ) {
    // If we navigated to this page, we will have an item available as a nav param
    this.selectedTrack = navParams.get('track');
  }

  ionViewWillEnter() {
   /*  this.storage.get('playlist').then((val) => {
      if(val != null) {
        let similar to var
        //but scope only in this block
        this.playlist = JSON.parse(val);

        //Check if current Track is already in Playlist
        this.checkTrack = this.playlist.map(x => x.id).indexOf(this.selectedTrack.id);
      }
    }); */
  }

  //Add Track to Playlist
  /* add(track: ITrackConstraint) {
    this.playlist.push(track);
    this.storage.set('playlist', JSON.stringify(this.playlist));
    
    //Update Buttons
    this.checkTrack = this.playlist.map(x => x.id).indexOf(this.selectedTrack.id);

    //Set Event for adding Track
    this.events.publish('addTrack', this.playlist);
  } */
  
  //AudioPlayer Control Functions
 /*  play(track: ITrackConstraint, index: number) {
      this.currentTrack = track;
      this.currentIndex = index;
  }
 */
  /* prev() {
    //Check Availability of previous Track
    if(this.playlist.length > 0 && this.currentIndex > 1) {
      //Set Counter + Track to previous
      let i = this.currentIndex - 1;
      let track = this.playlist[i];

      //Play next Track
      this.play(track, i);

      //Update UI on Changes
      this._cdRef.detectChanges();
    } else if(this.currentIndex == -1 && this.playlist.length > 0) {
      //Play 1. Track if none plays
      this.play(this.playlist[0], 0);
    }
  }

  next() {
    //Check Availability of next Track
    if(this.playlist.length > 0 && this.currentIndex >= 0 && this.currentIndex < this.playlist.length - 1) {
      //Set Counter + Track to next
      let i = this.currentIndex + 1;
      let track = this.playlist[i];
      
      //Play next Track
      this.play(track, i);

      //Update UI on Changes
      this._cdRef.detectChanges();
    } else if(this.currentIndex == -1 && this.playlist.length > 0) {
      //Play 1. Track if none plays
      this.play(this.playlist[0], 0);
    }
  }

  //When finished, play next Track
  onTrackFinished(track: any) { this.next(); }

  //Clear whole Playlist
  clear() {
    this.playlist = [];
    this.storage.remove('playlist');

    //Update Buttons
    this.checkTrack = this.playlist.map(x => x.id).indexOf(this.selectedTrack.id);
  }

  //Manually clear Tracks
  removeTrack(index: number) {
    this.playlist.splice(index, 1);
    this.storage.set('playlist', JSON.stringify(this.playlist));

    //Update Buttons
    this.checkTrack = this.playlist.map(x => x.id).indexOf(this.selectedTrack.id);
  } */
}
