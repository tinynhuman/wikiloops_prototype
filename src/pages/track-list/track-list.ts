import { Component, ChangeDetectorRef } from '@angular/core';
/* import { ITrackConstraint } from 'ionic-audio'; */
import { NavController, NavParams } from 'ionic-angular';
//import { Storage } from '@ionic/storage';

import { TracksProvider } from '../../providers/tracks/tracks';
import { TrackDetailPage } from '../track-detail/track-detail';

@Component({
  selector: 'page-home',
  templateUrl: 'track-list.html'
})
export class TrackListPage {
  tracklist: any;
  //playlist: ITrackConstraint[] = [];
  currentIndex: number = -1;
  //currentTrack: ITrackConstraint;

  //inject dependency of TracksProvider
  constructor(
    //private _cdRef: ChangeDetectorRef,
    public navCtrl: NavController,
    public navParams: NavParams,
    //private storage: Storage,
    private tracksProvider: TracksProvider
  ) {}

  //when component is loaded
  ionViewWillEnter() {
/*     this.storage.get('playlist').then((val) => {
      if(val != null) {
        //change string to json for location-object
      this.playlist = JSON.parse(val);
      }
    }); */

    //subscribe allows to access returned data
    this.tracksProvider.getTracks().subscribe(tracklist => { this.tracklist = tracklist.tracks; });
  }

  trackTapped(event, track) { this.navCtrl.push(TrackDetailPage, { track: track }); }
}
